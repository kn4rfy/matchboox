package co.uLimit.Matchboox;

import android.content.Intent;

import com.facebook.CallbackManager;
import com.facebook.react.ReactActivity;
import co.facebook.reactnative.androidsdk.FBSDKPackage;
import co.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ReactActivity {

  CallbackManager mCallbackManager;

  @Override
  protected String getMainComponentName() {
    return "Matchboox";
  }

  @Override
  protected boolean getUseDeveloperSupport() {
    return BuildConfig.DEBUG;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    mCallbackManager.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  protected List<ReactPackage> getPackages() {
    mCallbackManager = new CallbackManager.Factory().create();
    ReactPackage packages[] = new ReactPackage[]{
      new MainReactPackage(),
      new FBSDKPackage(mCallbackManager),
      new VectorIconsPackage()
    };
    return Arrays.<ReactPackage>asList(packages);
  }
}
