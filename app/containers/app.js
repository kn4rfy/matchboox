'use strict';

import React, {Component} from 'react';
import {AsyncStorage, Linking, Text, View, TouchableOpacity} from 'react-native';
import firebase from 'firebase/app';
import database from 'firebase/database';
import {Actions, Scene, Router} from 'react-native-router-flux';
import MainView from '../components/main/MainView';
import LogIn from '../components/login/LogIn';
import Settings from '../components/settings/Settings';
var Icon = require('react-native-vector-icons/MaterialIcons');
import styles from '../styles/styles';

const LOGIN_URL = "https://matchboox.herokuapp.com/api/auth/goodreads";
const HOME_URL = "https://matchboox.herokuapp.com";

class Button extends Component{
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={this.props.style}>
        <Icon name={this.props.name} size={this.props.size} color={this.props.color}/>
      </TouchableOpacity>
    )
  }
}

export default class Matchboox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: true
    };
    firebase.initializeApp({
      apiKey: "AIzaSyCGxLNxUFOcTZf69Pt79kfGZXF9eY-L0SQ",
      authDomain: "project-6242563380369753175.firebaseapp.com",
      databaseURL: "https://project-6242563380369753175.firebaseio.com",
      storageBucket: "project-6242563380369753175.appspot.com",
    });
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  componentDidMount() {
    Linking.addEventListener('url', this._handleOpenURL.bind(this));
  }

  async _loadInitialState() {
    await AsyncStorage.getItem('authenticated', (err, result) => {
      console.log(result);
      if (result !== null){
        this.setState({authenticated: result});
      }
      if (err) {
        console.log('AsyncStorage error: ' + err.message);
      }
    });
  }

  _handleOpenURL(event) {
    console.log(event.url)
    Linking.removeEventListener('url', this._handleOpenURL.bind(this));
  }

  _signUp() {
    fetch('https://matchboox.herokuapp.com/api/signup')
    .then((response) => response.text())
    .then((responseText) => {
      responseText = JSON.parse(responseText)
      this.setState({url: 'https://matchboox.herokuapp.com/api/auth/goodreads?token=' + responseText.token});
      Linking.openURL('https://matchboox.herokuapp.com/api/auth/goodreads?token=' + responseText.token);
    })
    .catch((error) => {
      console.warn(error);
    });
  }

  getLeftButton() {
    return(
      <Button style={styles.buttonHeader} onPress={Actions.Settings} name="settings" size={32} color="#AAAAAA" />
    )
  }

  getRightButton() {
    return(
      <Button style={styles.buttonHeader} onPress={Actions.Settings} name="library-books" size={32} color="#AAAAAA" />
    )
  }

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="LogIn" component={LogIn} title="Log In" />
          <Scene key="MainView"
            component={MainView}
            title="Matchboox"
            initial={true}
            renderLeftButton={this.getLeftButton}
            renderRightButton={this.getRightButton}
            navigationBarStyle={styles.buttonHeaderContainer}
          />
          <Scene key="Settings" component={Settings} title="Settings" />
        </Scene>
      </Router>
    );
  }

  // render() {
  //   return (
  //     <View style={styles.app}>
  //       { this.state.authenticated ? (
  //         <MainView />
  //       ) : (
  //         <SignUp />
  //       )}
  //     </View>
  //   );
  // }
}
