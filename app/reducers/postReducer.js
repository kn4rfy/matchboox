import * as types from '../actions/actionTypes';

export default function postReducer(state, action = {}) {
  switch (action.type) {
    case types.LIKE:
      return {
        ...state,
        likeCount: state.likeCount + 1
      };
    case types.DISLIKE:
      return {
        ...state,
        likeCount: state.likeCount - 1
      };
    case types.SHARE:
      return {
        ...state,
        shareCount: state.shareCount + 1
      };
    case types.COMMENT:
      return {
        ...state,
        commentCount: state.commentCount + 1
      };
    default:
      return state;
  }
}
