'use strict';

import React, {Component} from 'react';
import {View, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';
import FBSDK, {LoginButton,AccessToken} from 'react-native-fbsdk';

import LogIn from '../login/LogIn';

import styles from '../../styles/styles';

export default class Settings extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      authenticated: false
    };
  }

  async _login(data) {
    await AsyncStorage.setItem('authenticated', 'true', (err) => {
      if (err) {
        console.log('AsyncStorage error: ' + err.message);
      }
    });

    this.setState({
      authenticated: true
    });

    firebase.database().ref('users/' + data.userID).update({userID: data.userID});
  }

  async _logout() {
    await AsyncStorage.removeItem('authenticated', (err) => {
      if (err) {
        console.log('AsyncStorage error: ' + err.message);
      }
    });

    this.setState({
      authenticated: false
    });
  }

  render() {
    return (
      <View style={styles.app}>
        <LoginButton
          publishPermissions={["publish_actions"]}
          onLoginFinished={
            (error, result) => {
              if (error) {
                alert("login has error: " + result.error);
              } else if (result.isCancelled) {
                alert("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => this._login(data)
                )
              }
            }
          }
          onLogoutFinished={() => this._logout()}/>
      </View>
    )
  }
}
