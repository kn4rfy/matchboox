'use strict';

import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import Settings from '..//settings/Settings';
var Icon = require('react-native-vector-icons/MaterialIcons');

import SwipeCards from './SwipeCards';
import styles from '../../styles/styles';

class Card extends Component{
  render() {
    return (
      <View style={styles.card}>
        <Image style={styles.thumbnail} source={this.props.image} />
        <Text style={styles.text}>This is card {this.props.name}</Text>
      </View>
    )
  }
}

class NoMoreCards extends Component{
  render() {
    return (
      <View style={styles.noMoreCards}>
        <Text>No more cards</Text>
      </View>
    )
  }
}

class Button extends Component{
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={this.props.style}>
        <Icon name={this.props.name} size={this.props.size} color={this.props.color}/>
      </TouchableOpacity>
    )
  }
}

class ActionButtons extends Component{
  render() {
    return (
      <View style={styles.buttonFooterContainer}>
        <Button onPress={this.props.back} style={styles.buttonFooter} name="history" size={32} color="#AAAAAA" />
        <Button onPress={this.props.left} style={styles.buttonFooter} name="sentiment-dissatisfied" size={32} color="red" />
        <Button onPress={this.props.right} style={styles.buttonFooter} name="sentiment-satisfied" size={32} color="green" />
        <Button onPress={this.props.up} style={styles.buttonFooter} name="favorite-border" size={32} color="purple" />
      </View>

    )
  }
}

const Cards = [
  {name: '1', image: require('../../styles/images/1.jpg')},
  {name: '2', image: require('../../styles/images/2.jpg')},
  {name: '3', image: require('../../styles/images/3.jpg')},
  {name: '4', image: require('../../styles/images/4.jpg')},
  {name: '5', image: require('../../styles/images/5.jpg')},
  {name: '6', image: require('../../styles/images/6.jpg')},
  {name: '7', image: require('../../styles/images/7.jpg')},
  {name: '8', image: require('../../styles/images/8.jpg')},
  {name: '9', image: require('../../styles/images/9.jpg')},
  {name: '10', image: require('../../styles/images/10.jpg')},
  {name: '11', image: require('../../styles/images/11.jpg')},
  {name: '12', image: require('../../styles/images/12.jpg')},
  {name: '13', image: require('../../styles/images/13.jpg')},
  {name: '14', image: require('../../styles/images/14.jpg')},
  {name: '15', image: require('../../styles/images/15.jpg')},
  {name: '16', image: require('../../styles/images/16.jpg')},
  {name: '17', image: require('../../styles/images/17.jpg')},
];

export default class MainView extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      cards: Cards,
      outOfCards: false
    }
  }

  handleUp(card) {
    console.log("up");
  }

  handleRight(card) {
    console.log("right");
  }

  handleLeft(card) {
    console.log("left");
  }

  settings() {
    return (
      <Settings />
    );
  }

  cardRemoved(index) {
    console.log(`The index is ${index}`);

    let CARD_REFRESH_LIMIT = 3;

    if (this.state.cards.length - index <= CARD_REFRESH_LIMIT + 1) {
      console.log(`There are only ${this.state.cards.length - index - 1} cards left.`);

      this.setState({
        outOfCards: true
      });

      if (this.state.outOfCards) {
        console.log(`Adding ${Cards.length} more cards`);

        this.setState({
          cards: this.state.cards.concat(Cards),
          outOfCards: false
        });
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <SwipeCards
          cards={this.state.cards}

          renderCard={(cardData) => <Card {...cardData} />}
          renderNoMoreCards={() => <NoMoreCards />}
          actionButtons={(back,left,right,up) => <ActionButtons back={back} left={left} right={right} up={up} />}

          handleUp={this.handleUp.bind(this)}
          handleRight={this.handleRight.bind(this)}
          handleLeft={this.handleLeft.bind(this)}
          cardRemoved={this.cardRemoved.bind(this)}

          showButtons={true}
        />
      </View>
    )
  }
}
