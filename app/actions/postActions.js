import * as types from './actionTypes';

export function like() {
  return {
    type: types.LIKE
  };
}

export function dislike() {
  return {
    type: types.DISLIKE
  };
}

export function share() {
  return {
    type: types.SHARE
  };
}

export function comment() {
  return {
    type: types.COMMENT
  };
}
